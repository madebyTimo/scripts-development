#!/usr/bin/env bash
set -e

DEPENDENCIES=(docker git)
REPOSITORY_NAME="$(basename "$PWD")"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0")"
        echo "Run static code analysis on current path"
        exit
    fi
done

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

FAIL=false
GIT_BRANCH=""
if [[ -d .git ]]; then
    GIT_BRANCH="$(git branch --show-current)"
fi

# All languages
docker run --pull always --rm \
    --volume "${PWD}:/media/workdir" \
    madebytimo/semgrep || \
    FAIL=true

# Java
if [[ -f pom.xml ]]; then
    if [[ -n "$SONAR_HOST_URL" ]] && [[ -n "$SONAR_TOKEN" ]]; then
        docker run --pull always --rm \
            --workdir "/media/workdir" \
            --volume "${REPOSITORY_NAME}-build-cache:/root/.m2" \
            --volume "${PWD}:/media/workdir" \
            madebytimo/java-maven \
            mvn clean verify sonar:sonar \
                -Dsonar.branch.name="$GIT_BRANCH" \
                -Dsonar.host.url="$SONAR_HOST_URL" \
                -Dsonar.projectKey="$REPOSITORY_NAME" \
                -Dsonar.projectName="$REPOSITORY_NAME" \
                -Dsonar.token="$SONAR_TOKEN" || \
        FAIL=true
    fi
fi

if [[ "$FAIL" == true ]]; then
    exit 1
fi
