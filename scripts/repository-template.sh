#!/usr/bin/env bash
set -e

DEPENDENCIES=(curl replace-vars.sh tar)
DOMAIN="example.madebytimo.de"
OUTPUT_DIR="$PWD"
PACKAGE_URL="https://gitlab.com/madebyTimo/repository-template/-/archive/main/\
repository-template-main.tar.gz"
REPLACE=false
REPOSITORY_NAME="$(basename "$PWD")"
TEMPLATE_PATHS=()
WORKDIR="${PWD}/.temp-$(basename "$0")"
LIST_ONLY=false

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0") ARGUMENTS PATH..."
        echo "Places the template-repository files in the current repository."
        echo "ARGUMENTS are the template paths to use, or"
        echo "    --domain DOMAIN The domain to use, default is \"${DOMAIN}\"."
        echo "    --list To only list available files"
        echo "    --replace To replace existing files"
        echo "    --repository-name NAME The name of the repository to use, default is the" \
            "basename of the current folder"
        exit
    fi
done

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" = "--domain" ]]; then
        shift
        DOMAIN="$1"
    elif [[ "$1" = "--list" ]]; then
        LIST_ONLY=true
    elif [[ "$1" == "--repository-name" ]]; then
        shift
        REPOSITORY_NAME="$1"
    elif [[ "$1" = "--replace" ]]; then
        REPLACE=true
    else
        TEMPLATE_PATHS+=("$1")
    fi
    shift
done

TEMPLATE_PATHS=("${TEMPLATE_PATHS[@]#/}")
TEMPLATE_PATHS=("${TEMPLATE_PATHS[@]%/}")
if [[ -z "${TEMPLATE_PATHS[*]}" ]]; then
    if [[ "$LIST_ONLY" == true ]]; then
        TEMPLATE_PATHS+=(".")
    else
        echo "No path for template files given"
        exit 1
    fi
fi

if [[ -e "$WORKDIR" ]]; then
    echo "\"${WORKDIR}\" already exists. Remove in 10 seconds."
    sleep 10
    rm -f -r "$WORKDIR"
fi
mkdir "$WORKDIR"
cd "$WORKDIR"

function callOnlyWithVariables() {
    env --ignore-environment PATH="$PATH" \
        DOMAIN="$DOMAIN" \
        REPOSITORY_NAME="$REPOSITORY_NAME" \
        APPLICATION_NAME="$APPLICATION_NAME" \
        "$@"
}

curl --silent --location "$PACKAGE_URL" \
    | tar --extract --gzip --strip-components 2 --wildcards '*/templates'

APPLICATION_NAME="${REPOSITORY_NAME#docker-}"
echo "Used variables:"
callOnlyWithVariables env | sed "/^PATH=/d"
echo

for TEMPLATE_PATH in "${TEMPLATE_PATHS[@]}"; do
    mapfile -t FILES_IN_DIR -d '' < <(find "$TEMPLATE_PATH" -type f -or -type l)
    for FILE in "${FILES_IN_DIR[@]}"; do
        if [[ "$LIST_ONLY" == true ]]; then
            echo "${FILE#./}"
        else
            OUTPUT_DIR_FOR_TEMPLATE_PATH="$OUTPUT_DIR"
            if [[ "$FILE" == builder/* ]]; then
                OUTPUT_DIR_FOR_TEMPLATE_PATH+="/builder"
            elif [[ "$FILE" == ci/github/* ]]; then
                OUTPUT_DIR_FOR_TEMPLATE_PATH+="/.github/workflows"
            elif [[ "$FILE" == devcontainer/* ]]; then
                OUTPUT_DIR_FOR_TEMPLATE_PATH+="/.devcontainer"
            elif [[ "$FILE" == docker/container/*.sh ]]; then
                OUTPUT_DIR_FOR_TEMPLATE_PATH+="/files"
            elif [[ "$FILE" == go/main.go ]]; then
                OUTPUT_DIR_FOR_TEMPLATE_PATH+="/cmd/$APPLICATION_NAME"
            elif [[ "$FILE" == tester/* ]]; then
                OUTPUT_DIR_FOR_TEMPLATE_PATH+="/tester"
            fi
            OUTPUT_FILENMAME="$(basename "$FILE")"
            if [[ "$FILE" == go/main.go ]]; then
                OUTPUT_FILENMAME="${APPLICATION_NAME}.go"
            fi
            OUTPUT_FILE="${OUTPUT_DIR_FOR_TEMPLATE_PATH}/${OUTPUT_FILENMAME}"
            mkdir --parents "$OUTPUT_DIR_FOR_TEMPLATE_PATH"
            if [[ -f "$OUTPUT_FILE" ]] && [[ "$REPLACE" != true ]]; then
                echo "\"${OUTPUT_FILE}\" already exists, skipping."
            else
                echo "Add file \"${OUTPUT_FILE}\""
                callOnlyWithVariables replace-vars.sh "$FILE" >"$OUTPUT_FILE"
                if [[ "$OUTPUT_FILE" == *.sh ]]; then
                    chmod +x "$OUTPUT_FILE"
                fi
            fi
        fi
    done
done

rm -f -r "$WORKDIR"
