#!/usr/bin/env bash
set -e

DEPENDENCIES=(npm)
NEW_VERSION=""
OLD_VERSION=""
VERSION_IN_FILE=false
WORKING_DIR="${PWD}/.temp-$(basename "$0")"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0") [ARGUMENT]"
        echo "Check if the version is incremented correctly by one step."
        echo "ARGUMENT can be"
        echo "    --file Versions are files, not plain Versions."
        echo "    --new VERSION The new version."
        echo "    --old VERSION The old version."
        exit
    fi
done

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" == "--file" ]]; then
        VERSION_IN_FILE=true
    elif [[ "$1" == "--new" ]]; then
        shift
        NEW_VERSION="$1"
    elif [[ "$1" == "--old" ]]; then
        shift
        OLD_VERSION="$1"
    else
        echo "Unknown argument: \"$1\""
        exit 1
    fi
    shift
done

if [[ -z "$NEW_VERSION" ]]; then
    echo "Please specify new version."
    exit 1
elif [[ -z "$OLD_VERSION" ]]; then
    echo "Please specify old version."
    exit 1
fi

if [[ "$VERSION_IN_FILE" == true ]]; then
    NEW_VERSION="$(cat "$NEW_VERSION")"
    OLD_VERSION="$(cat "$OLD_VERSION")"
fi

if [[ -e "$WORKING_DIR" ]]; then
    echo "\"${WORKING_DIR}\" exists already. Removing in 10 seconds."
    sleep 10
    rm -f -r "$WORKING_DIR"
fi
mkdir "$WORKING_DIR"
cd "$WORKING_DIR"

npm install semver

CORRECT_VERSION_INCREMENT=false
for INCREMENT_LEVEL in major minor patch; do
    INCREMENTED_VERSION="v$(npx semver --increment "$INCREMENT_LEVEL" "$OLD_VERSION")"
    if [[ "$NEW_VERSION" == "$INCREMENTED_VERSION" ]]; then
        CORRECT_VERSION_INCREMENT=true
    fi
done

rm -f -r "$WORKING_DIR"

if [[ "$CORRECT_VERSION_INCREMENT" == true ]]; then
    echo "Version increment from \"${OLD_VERSION}\" to \"${NEW_VERSION}\" is correct".
else
    echo "Version increment from \"${OLD_VERSION}\" to \"${NEW_VERSION}\" is not correct".
    exit 10
fi
